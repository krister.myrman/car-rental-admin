import {create, read, update, remove} from "./crud";
import {cars} from "./stores";

const API_URL = "http://localhost:8081/api/v1/cars/";

export async function deleteCar(id) {
    await remove(API_URL, id);
    await getCars();
}

export async function createCar(car) {
    await create(API_URL, car);
    await getCars();
}

export async function updateCar(car) {
    await update(API_URL, car);
    await getCars();
}

export async function getCars() {
    cars.set(await read(API_URL));
}