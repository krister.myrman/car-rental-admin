import Cars from "./routes/Cars.svelte";
import Customers from "./routes/Customers.svelte";
import NotFound from "./routes/NotFound.svelte";
import Home from "./routes/Home.svelte";

export default {
    '/': Home,
    '/cars': Cars,
    '/customers': Customers,
    '*': NotFound
}