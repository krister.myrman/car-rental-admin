import {writable} from "svelte/store";

export const loggedIn = writable(false);
export const cars = writable([]);
export const customers = writable([]);
