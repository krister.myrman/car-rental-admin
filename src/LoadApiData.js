export async function loadApiData(url, store) {
    const response = await fetch(url, {
        method: 'GET',
        credentials: 'include'
    });
    let data = await response.json();
    store.set(data);
}